package assignments;
import java.util.ArrayList;
import com.google.gson.GsonBuilder;


public class Main {
	
	

	public static void main(String[] args) {
		ArrayList<Block> blockchain = new ArrayList<Block>();
		int difficulty = 5;
		blockchain.add(new Block("1st block", "0"));
		System.out.println("Try to mine block 1");
		blockchain.get(0).mineBlock(difficulty);
		
		blockchain.add(new Block("2nd block",(blockchain.get(blockchain.size()-1).hash))); 
		System.out.println("Try to mine block 2");
		blockchain.get(1).mineBlock(difficulty);
		
		
		blockchain.add(new Block("3rd block",(blockchain.get(blockchain.size()-1).hash)));
		System.out.println("try to mine block 3");
		blockchain.get(2).mineBlock(difficulty);
		
		blockchain.add(new Block("4th block",(blockchain.get(blockchain.size()-1).hash)));
		System.out.println("try to mine block 4");
		blockchain.get(3).mineBlock(difficulty);
		
		String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);	
		System.out.println("\nThe block chain: ");
		System.out.println(blockchainJson);
	}
	public static Boolean isChainValid() {
		Block currentBlock; 
		Block previousBlock;
		int difficulty = 5;
		ArrayList<Block> blockchain = new ArrayList<Block>();
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		//loop through Block chain to check hashes:
		for(int i=1; i < blockchain.size(); i++) {
			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i-1);
			
			//To compare registered hash and calculated hash:
			if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
				System.out.println("Current Hashes not equal");			
				return false;
			}
			//To compare previous hash and registered previous hash
			if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
				System.out.println("Previous Hashes not equal");
				return false;
			}
			
			//check if hash is solved
			if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) {
				System.out.println("This block is not mined");
				return false;
			}
		}
		return true;
	}

}



